from yamlhelper import yaml_parse, yaml_dump
import os


def process_template(input_directory, filename, output_directory):
    with open(os.path.join(input_directory, filename)) as f:
        cfn = yaml_parse(f.read())

    for resource, config in cfn['Resources'].items():
        if config['Type'] == 'AWS::Lambda::Function':
            if isinstance(config['Properties']['Code'], str) and\
               config['Properties']['Code'].startswith('load_file='):
                script_file = config['Properties']['Code'].split('=')[-1]
                if script_file.endswith('.py'):
                    with open(script_file) as f:
                        config['Properties']['Code'] = {"ZipFile": f.read()}

    out_file = os.path.join(output_directory, filename.replace('_template', ''))
    with open(out_file, 'w') as f:
        f.write(yaml_dump(cfn))


if __name__ == '__main__':
    directory = './squeegee/cfn-templates/'
    for file in os.listdir(directory):
        if file.endswith("_template.yaml"):
            process_template(directory, file, './deployment/templates/')
