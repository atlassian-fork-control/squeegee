import os
import boto3
import logging
import sys
import json
import re


logging.basicConfig()
manifest_key_regex = r'.*\/?(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})-[0-9]{8}\/[^\/]*\.json$'


def validate_environment_variables(logger, manifest_bucket, manifest_key, cur_bucket):
    if not manifest_bucket or not manifest_key:
        logger.warning('Unable to get bucket or key from event')
        return False

    if manifest_bucket != cur_bucket:
        logger.warning('Expected manifest bucket to be {} got {}'.format(cur_bucket, manifest_bucket))
        return False

    if not validate_manifest_key(manifest_key):
        logger.warning('Manifest {} does not match expected pattern'.format(manifest_key))
        return False
    return True


def validate_manifest_key(manifest_key):
    if re.match(manifest_key_regex, manifest_key):
        return True
    return False


def get_bucket_details_from_event(event, logger):
    logger.info('Getting bucket details from event %s', json.dumps(event))
    result = (None, None)
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                return get_bucket_details_from_event(json.loads(record['Sns']['Message']), logger)
            elif 's3' in record:
                if 'bucket' in record['s3'] and \
                                'name' in record['s3']['bucket'] and \
                                'object' in record['s3'] and \
                                'key' in record['s3']['object']:
                    result = (record['s3']['bucket']['name'], record['s3']['object']['key'])
    return result


def handler(event, context):
    if os.environ.get('BATCH_JSON_LOGGING', 'false') == 'true':
        json_logging = True
    else:
        json_logging = False

    cur_bucket = os.environ.get('CUR_BUCKET')
    batch_job_definition = os.environ.get('BATCH_JOB_DEFINITION')
    batch_job_queue = os.environ.get('BATCH_JOB_QUEUE')
    batch_region = os.environ.get('BATCH_REGION', 'us-east-1')

    # Initialise Logging
    logger = logging.getLogger()
    logger.handlers = []
    h = logging.StreamHandler(sys.stdout)
    log_format = '%(asctime)s - ' + context.aws_request_id + ' - %(name)s - %(levelname)s - %(message)s'
    h.setFormatter(logging.Formatter(log_format))
    logger.addHandler(h)
    logger.setLevel(logging.DEBUG)
    ######
    logger.info(event)

    manifest_bucket, manifest_key = get_bucket_details_from_event(event, logger)
    if not validate_environment_variables(logger, manifest_bucket, manifest_key, cur_bucket):
        return False

    command = ['cur-data', '-m', 'init-workers']
    if json_logging:
        command.insert(0, '--json-logging')

    client = boto3.client('batch', region_name=batch_region)
    response = client.submit_job(
            jobName='CURTransform',
            jobQueue=batch_job_queue,
            jobDefinition=batch_job_definition,
            containerOverrides={
                'vcpus': 2,
                'memory': 8192,
                'command': command,
                'environment': [
                    {'name': 'MANIFEST_BUCKET', 'value': manifest_bucket},
                    {'name': 'MANIFEST_KEY', 'value': manifest_key},
                    {'name': 'BATCH_JOB_DEFINITION', 'value': batch_job_definition},
                    {'name': 'BATCH_REGION', 'value': batch_region}
                ]
            },
            retryStrategy={'attempts': 2}
    )

    logger.info(response)
    if 'jobId' in response:
        return True
    return False
