import sys
import os
import argparse
from curdata.batch_handler import runner as cur_data_runner
from curdata.batch_handler import parser as cur_data_parser
from pricingdata.batch_handler import runner as pricing_data_runner
from pricingdata.batch_handler import parser as pricing_data_parser
from cloudwatchdata.batch_handler import runner as cloudwatch_data_runner
from cloudwatchdata.batch_handler import parser as cloudwatch_data_parser
from logging_formatters import JSONFormatter
import logging

logging.basicConfig()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug",
                        help="Enable debug logging",
                        action="store_true"
                        )
    parser.add_argument("-j", "--json-logging",
                        help="Enable JSON logging",
                        action="store_true"
                        )
    subparsers = parser.add_subparsers(title="commands", dest="command")
    cur_data_parser(subparsers.add_parser('cur-data'))
    pricing_data_parser(subparsers.add_parser('pricing-data'))
    cloudwatch_data_parser(subparsers.add_parser('cloudwatch-data'))
    return parser.parse_args()


def initialise_logging(args):
    initialise_logger = logging.getLogger()
    initialise_logger.handlers = []
    h = logging.StreamHandler(sys.stdout)
    if args.json_logging:
        log_formatter = JSONFormatter()
    else:
        log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        log_formatter = logging.Formatter(log_format)

    h.setFormatter(log_formatter)
    initialise_logger.addHandler(h)
    if args.debug:
        initialise_logger.setLevel(logging.DEBUG)
    else:
        initialise_logger.setLevel(logging.INFO)
    return initialise_logger


def main(args):
    logger.info(os.environ)
    if args.command == 'cur-data':
        logger.info('Starting cur-data stack runner')
        return cur_data_runner(args)
    elif args.command == 'pricing-data':
        logger.info('Starting pricing-data stack runner')
        return pricing_data_runner(args)
    elif args.command == 'cloudwatch-data':
        logger.info('Starting cloudwatch-data stack runner')
        return cloudwatch_data_runner(args)


if __name__ == '__main__':
    arguments = parse_args()
    logger = initialise_logging(arguments)
    response = main(arguments)
    if not response:
        sys.exit(1)
    sys.exit(0)
